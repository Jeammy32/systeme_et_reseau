import sys
import math

def main():
    ipAddress = ""
    mask = ""
    if len(sys.argv) == 2 :
        ipAddress = sys.argv[1]
        print(decToBin(ipAddress))
    if len(sys.argv) == 4 :
        firstIpAddress = sys.argv[1]
        mask = sys.argv[2]
        secondIpAddress = sys.argv[3]
        if checkIfInSameNetwork(firstIpAddress,mask,secondIpAddress):
            print("Those two ip are in the same network!")
        else:
            print("Those two ip are not in the same network...")

def checkIfInSameNetwork(firstIpAddress,mask,secondIpAddress):
    firstIpAddress = decToBin(firstIpAddress).replace(".","")
    mask = mask.replace("/","")
    secondIpAddress = decToBin(secondIpAddress).replace(".","")
    for bite in range(0,int(mask)):
        if firstIpAddress[bite] != secondIpAddress[bite]:
            return False
    return True

def decToBin(ipAddress):
    binaryResult = ""
    ipAddressByBytes = ipAddress.split(".")
    for i, value in enumerate(ipAddressByBytes):
        isLastValue = i == len(ipAddressByBytes)-1
        i += 1
        byte = ""
        while float(value) > 0:
            byte = str(int(value)%2) + byte
            value = math.floor(int(value)/2)
        if len(byte) != 8:
            while len(byte) != 8:
                byte = "0" + byte
        if isLastValue == False:
            byte += "."
        binaryResult += byte
    return binaryResult

main()

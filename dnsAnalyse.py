from scapy.all import *

DNSRequest = 0
DNSResponse = 0
domainNames = []
ipList = []

packets = rdpcap('trace.pcapng')

for packet in packets:
  if packet.haslayer(DNS):
    DNSRequest+=1
    if packet.getlayer(DNS).qr == 1:
      DNSResponse+=1
      for i in range(packet[DNS].ancount):
        answer = packet[DNS].an[i]
        if answer not in domainNames:
          domainNames.append(answer.rrname)
          ipList.append(answer.rdata)
for i in range(len(domainNames)):
  print("Domain name: {}".format(domainNames[i]))
  print("IP List : {}".format(ipList[i]))
  

#print(DNSRequest)
#print(DNSResponse)
